import { MySqlContainer } from "@testcontainers/mysql";

describe("Testcontainers", () => {
  it("should start a mysql container", async () => {
    const container = await new MySqlContainer().start();
    console.log(`Container started: ${container.getId()}`);
    await container.stop();
  }, 60000);
});
